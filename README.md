# README #


*INTRODUCTION*

The service exposes two endpoints, one to allocate an IP address and the second endpoint is heartbeat check. A brief explanation of how things work.
	Allocate endpoint is meant to allocate a new IP address to a new user.
	HeartBeat endpoint is meant to check the validity of IP address of existing user and renew IP in case it's expired.

There are three configurational settings provided in the resources - 

	1. ip.range.min
	2. ip.range.max
	3. ip.timeout

All IP addresses are to be in the format: a.b.c.d, where, a, b, c, d lie in between 0 and 255. By providing a min and max IP, the service generates a list of all the IP addresses in between and makes them available for allocation.
What is meant by "the IP addresses in between" !? Like we add any two numbers, we are assuming the same behaviour with IP addresses, so for 10.0.0.1, next IP address will be 10.0.0.2, similarly, for 10.255.255.255, next will be 11.0.0.0. This makes a 255 x 255 x 255 x 255 number of possible IP addresses.

Now that you know, the service works on incremental logic, please make sure your max IP is greater than min IP (will put in a validation for that later #TODO).

A unique (IP address should be bound to mac address to maintain uniqueness) IP address is allocated to each user. The IP address allocated is expired as per the ip.timeout setting and on the subsequent hearbeat API call, a new IP address is allocated to the user.
Expiration and reallocation of IP addresses is only dependent on heartbeat API. 


*IMPLEMENTATION*

The service is implemented using Spring Boot Framework and Redis Caching service. On application startup, the list of IP addresses is created using the min and max IP settings, and is maintained in cache. Free IPs are stored as List data structure and Allocated IPs are stored as Map data structure inside Redis. 
As a new allocation request is received, an IP is popped from free IP addresses and is added in allocated IP map with the timestamp of the instant it is allocated.
On every heartbeat request, the IP timestamp is verified from the allocated IP map in cache, in case the IP is expired, the same IP is added back to the free IP list and an IP from list is put into alllocated IP map with current timestamp.



Use command *mvn spring-boot:run* to run the project from terminal.

Use Postman Collection for testing: https://www.getpostman.com/collections/098b55a077a43b0d7364
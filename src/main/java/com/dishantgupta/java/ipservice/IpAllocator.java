package com.dishantgupta.java.ipservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;

@Service
public class IpAllocator {

    @Autowired
    private RedisTemplate<String, String> template;

    @Autowired
    IpFetcher ipFetcher;

    @Value("${ip.timeout}")
    String expiryTimedelta;

    public static String ALLOCATED_IP_MAP_KEY = "allocated_ip_map_key";

    public void allocate(ServiceIP ip){
        Instant start = Instant.now();
        template.opsForHash().put(ALLOCATED_IP_MAP_KEY, ip.ip, String.valueOf(start.toEpochMilli()));
    }

    public boolean isAllocated(ServiceIP ip){
        return template.opsForHash().hasKey(ALLOCATED_IP_MAP_KEY, ip.ip);
    }

    public void unallocate(ServiceIP ip){
        template.opsForHash().delete(ALLOCATED_IP_MAP_KEY, ip.ip);
    }

    public ServiceIP heartbeat(ServiceIP ip) throws IpValidationException {
        String epoch = template.opsForHash().get(ALLOCATED_IP_MAP_KEY, ip.ip).toString();
        ServiceIP newIp = ip;
        Instant start = Instant.now();
        Instant recorded = Instant.ofEpochMilli(Long.valueOf(epoch));
        if (Duration.between(recorded, start).getSeconds() > Integer.valueOf(expiryTimedelta)) {
            unallocate(ip);
            ipFetcher.putIp(ip);
            newIp = ipFetcher.getIp();
            allocate(newIp);
        }
        return newIp;
    }
}

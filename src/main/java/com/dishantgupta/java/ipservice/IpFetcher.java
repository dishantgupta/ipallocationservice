package com.dishantgupta.java.ipservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

import static com.dishantgupta.java.ipservice.IpAllocator.ALLOCATED_IP_MAP_KEY;
import static com.dishantgupta.java.ipservice.UserIpValidator.USER_IP_MAP_KEY;

@Service
public class IpFetcher {

    @Autowired
    RedisTemplate<String, String> template;

    private static String FREE_IP_STACK_KEY = "free_ip_stack_key";

    // read from resources
    @Value("${ip.range.max}")
    private String maxIpAddr;

    @Value("${ip.range.min}")
    private String minIpAddr;

    @PostConstruct
    void init(){
        // convert maxIpAddr and minIpAddr to ServiceIP objects here
        // load all IP address range in REDIS free IP stack
        List<String> ipAddressRange = IpIndexer.getIpAddresses(new ServiceIP(minIpAddr), new ServiceIP(maxIpAddr));
        template.delete(FREE_IP_STACK_KEY);
        template.delete(ALLOCATED_IP_MAP_KEY);
        template.delete(USER_IP_MAP_KEY);
        template.opsForList().leftPushAll(FREE_IP_STACK_KEY, ipAddressRange);
    }

    public ServiceIP getIp() throws IpValidationException {
        // pop one IP from redis
        // add that IP in allocated IP table with timestamp

        String freeIpAddr = template.opsForList().leftPop(FREE_IP_STACK_KEY);
        if (freeIpAddr == null){
            throw new IpValidationException("No free IPs to allocate !!");
        }
        ServiceIP freeIp = new ServiceIP(freeIpAddr);
        return freeIp;
    }

    public void putIp(ServiceIP ip){
        template.opsForList().rightPush(FREE_IP_STACK_KEY, ip.ip);
    }
}

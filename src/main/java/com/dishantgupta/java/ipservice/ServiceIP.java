package com.dishantgupta.java.ipservice;

import java.util.Arrays;
import java.util.List;

public class ServiceIP {

    // ip address to be in form a.b.c.d, where a,b,c,d lies in between 0-255

    public String ip;
    private String a;
    private String b;
    private String c;
    private String d;

    public ServiceIP (String ipAddress){
        List<String> abcd = Arrays.asList(ipAddress.split("\\."));
        ip = ipAddress;
        a = abcd.get(0);
        b = abcd.get(1);
        c = abcd.get(2);
        d = abcd.get(3);
    }

    public ServiceIP getNextIP() {
        ServiceIP nextIp = new ServiceIP(this.ip);
        if ((Integer.valueOf(d) < 255)){
            nextIp.d = String.valueOf(Integer.valueOf(d) + 1);
        } else {
            if (Integer.valueOf(c) < 255) {
                nextIp.d = String.valueOf(0);
                nextIp.c = String.valueOf(Integer.valueOf(c) + 1);
            } else {
                if (Integer.valueOf(b) < 255) {
                    nextIp.d = String.valueOf(0);
                    nextIp.c = String.valueOf(0);
                    nextIp.b = String.valueOf(Integer.valueOf(b) + 1);
                } else {
                    if (Integer.valueOf(a) < 255) {
                        nextIp.d = String.valueOf(0);
                        nextIp.c = String.valueOf(0);
                        nextIp.b = String.valueOf(0);
                        nextIp.a = String.valueOf(Integer.valueOf(a) + 1);
                    }
                }
            }
        }
        nextIp.ip = String.format("%s.%s.%s.%s", nextIp.a, nextIp.b, nextIp.c, nextIp.d);
        return nextIp;
    }

    public String toString(){
        return this.ip;
    }
}

package com.dishantgupta.java.ipservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class UserIpValidator {

    @Autowired
    RedisTemplate<String, String> redisTemplate;

    public static String USER_IP_MAP_KEY = "user_ip_map_key";

    public void allocateIp(String macAddr, String ipAddr) {
        redisTemplate.opsForHash().put(USER_IP_MAP_KEY, macAddr, ipAddr);
    }

    public Boolean isAllocated(String macAddr){
        return redisTemplate.opsForHash().hasKey(USER_IP_MAP_KEY, macAddr);
    }

    public String getAllocatedIp(String macAddr){
        return redisTemplate.opsForHash().get(USER_IP_MAP_KEY, macAddr).toString();
    }

    public void updateAllocatedIp(String macAddr, String ipAddr){
        redisTemplate.opsForHash().put(USER_IP_MAP_KEY, macAddr, ipAddr);
    }
}

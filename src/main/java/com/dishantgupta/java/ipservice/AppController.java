package com.dishantgupta.java.ipservice;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class AppController {

    @Autowired
    UserIpValidator userIpValidator;

    @Autowired
    IpFetcher ipFetcher;

    @Autowired
    IpAllocator ipAllocator;

    @RequestMapping(method = RequestMethod.GET, path = "/ip/allocate/")
    public ResponseEntity allocateIp(@RequestParam("mac_addr") String macAddr){
        int status = 200;
        Object body = null;
        try {
            if (!userIpValidator.isAllocated(macAddr)){
                ServiceIP ip = ipFetcher.getIp();                   // get IP from list
                ipAllocator.allocate(ip);                           // registers timestamp with IP
                userIpValidator.allocateIp(macAddr, ip.ip);         // register IP with user
                body = ip.ip;
            } else {
                body = String.format(
                        "IP %s already allocated to user",
                        userIpValidator.getAllocatedIp(macAddr));
            }
        } catch (IpValidationException e) {
            status = 400;
            body = e.getMessage();
        }
        return ResponseEntity.status(status).body(body);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/ip/heartbeat/")
    public ResponseEntity heartbeat(@RequestParam("mac_addr") String macAddr, @RequestParam("ip_addr") String ipAddr){
        int status = 200;
        Object body = null;
        ServiceIP ip = new ServiceIP(ipAddr);
        try{
            // check if mac address is registered in system
            if(! userIpValidator.isAllocated(macAddr)){
                throw new IpValidationException("Unregistered user. Please use Allocate API first");
            }

            // check if correct IP is sent in request
            String allocatedIp = userIpValidator.getAllocatedIp(macAddr);
            if (!ipAddr.equals(allocatedIp)) {
                throw new IpValidationException("Difference in IP passed and IP registered in system.");
            }

            ip = ipAllocator.heartbeat(ip);
            userIpValidator.updateAllocatedIp(macAddr, ip.ip);
            body = ip.ip;
        } catch (IpValidationException e) {
            status = 400;
            body = e.getMessage();
        }
        return ResponseEntity.status(status).body(body);
    }
}

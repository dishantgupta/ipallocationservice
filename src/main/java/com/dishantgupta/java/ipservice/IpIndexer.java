package com.dishantgupta.java.ipservice;

import java.util.ArrayList;
import java.util.List;

public class IpIndexer {

    public static List<String> getIpAddresses(ServiceIP minIp, ServiceIP maxIp) {
        List<String> ips = new ArrayList<>();
        ServiceIP newIP = minIp;
        ips.add(newIP.toString());

        while(!newIP.ip.equals(maxIp.ip)) {
            newIP = newIP.getNextIP();
            ips.add(newIP.toString());
        }
        return ips;
    }
}

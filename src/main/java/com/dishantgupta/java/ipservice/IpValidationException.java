package com.dishantgupta.java.ipservice;

public class IpValidationException extends Exception {

    IpValidationException(String message){
        super(message);
    }
}
